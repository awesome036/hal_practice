<?php
require_once("./tcpdf/tcpdf.php");

// 日付設定
date_default_timezone_set('Asia/Tokyo');
$date = date("Y/m/d");

// POSTされた値を変数に格納
$zipcode = "〒".$_POST["zipcode"];
$address = $_POST["address"];
$name = $_POST["name"];
$products = $_POST["products"];

// // 合計金額と消費税計算
// $i = 0;
// $sum = 0;
// while($i < 5){
// 	$sum = $sum + ($products[$i]["price"] * $products[$i]["amount"]);
// 	$i++;
// }
// echo $sum;

// TCPDF生成
$tcpdf = new TCPDF();
$tcpdf->setFontSubsetting(false); // フォントの埋め込みをする
$tcpdf->SetFont('ipag', '', 14);
$tcpdf->AddPage();				// ページ追加

// メタ情報の追加
$tcpdf->SetAuthor("クロサワオサム");
$tcpdf->SetTitle("課題05");
$tcpdf->SetSubject("領収書");

// ロゴ画像出力
$file = "./images/hal.png";
$tcpdf->Image($file, 175, 18, 25, 25);

// 文字書き込み
$tcpdf->Write($h, $date, $link, $fill, "R", true);	// 日付出力
$tcpdf->Write($h, $zipcode, $link, $fill, $align, true);	// 郵便番号出力
$tcpdf->Write($h, $address, $link, $fill, $align, true);	// 住所出力
$tcpdf->setFontSize(24);
$tcpdf->Write($h, $name, $link, $fill, $align, false);	// 氏名出力
$tcpdf->setFontSize(14);
$tcpdf->Write(14, " 様", $link, $fill, $align, true);
$tcpdf->setX(130);
$tcpdf->Write($h, "〒160-0023", $link, $fill, $align, true);
$tcpdf->setX(130);
$tcpdf->Write($h, "東京都新宿区西新宿1-7-3", $link, $fill, $align, true);
$tcpdf->setX(130);
$tcpdf->Write($h, "TEL:03-3344-XXXX", $link, $fill, $align, true);
$tcpdf->setX(130);
$tcpdf->Write($h, "FAX:03-3344-XXXX", $link, $fill, $align, true);
$tcpdf->setY($tcpdf->getY() + 15);
$tcpdf->Write($h, "下記の通り、領収いたしました。", $link, $fill, $align, true);
$tcpdf->Ln();
$tcpdf->Write($h, "合計金額", $link, $fill, $align, false);
$tcpdf->Write($h, "円", $link, $fill, "C", true);





$tcpdf->Output("kadai05.pdf");		// PDF出力