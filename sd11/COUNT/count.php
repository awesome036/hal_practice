<!DOCTYPE html>
<html lang="ja">
	<head>
		<title>カウント</title>
		<meta charset="utf-8">
		<meta name="author" content="Osamu Kurosawa">
	</head>
	<body>
		<?php
			// セッションスタート
			session_start();

			if(isset($_POST["CLEAR"])){			// クリアボタンが押された場合
				// セッション内のCOUNTを削除
				unset($_SESSION["COUNT"]);

				print "<h1>クリアしました！</h1>";
			}else{
				// カウント変数
				$CNT = 0;

				// セッションにCOUNTの値があったら
				// COUNTの値をCNTに入れる
				if(isset($_SESSION["COUNT"])){
					// COUNTがある場合
					$CNT = $_SESSION["COUNT"];
				}

				if(isset($_POST["CNTUP"])){				// カウントUPボタンが押された場合
					// カウントUP
					$CNT++;
				}elseif(isset($_POST["CNTDWN"])){		// カウントDWNボタンが押された場合
					// カウントCNTDWN
					$CNT--;
				}

				// セッションにCOUNTの値を保存
				$_SESSION["COUNT"] = $CNT;

				print "<h1>".$_SESSION["COUNT"]."</h1>";
			}
		?>
		<a href="./input.html">戻る</a>
	</body>
</html>