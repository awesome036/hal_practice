<!DOCTYPE html>
<html>
	<head>
		<title>自己紹介</title>
		<meta charset="utf-8">
		<style>
			span {
				font-size: 30px;
				color: #0f0;
				text-shadow:	2px 2px 2px #666,
								-2px 2px 2px #666,
								2px -2px 2px #666,
								-2px -2px 2px #666;
			}
			.red {
				color: #f00;
			}
		</style>
	</head>
	<body>
		<h1>自己紹介</h1>

		<?php
			//姓名の入力チェック
			if(empty($_POST["Sei"]) && empty($_POST["Mei"])){
				print "<h2 class=\"red\">姓も名も入力されていません。</h2>\n";
			}else if(empty($_POST["Sei"])){
				print "<h2 class=\"red\">姓が入力されていません。</h2>\n";
			}else if(empty($_POST["Mei"])){
				print "<h2 class=\"red\">名が入力されていません。</h2>\n";
			}else{
				print "<h2>あなたの名前は<span>".$_POST["Sei"].$_POST["Mei"]."</span>さんですね。</h2>\n";
			}

			//性別の取得処理(ラジオボタンの値を取得)
			//"0"か"1"数字を取得
			//"0"なら「男性ですね。」と表示
			//"1"なら「女性ですね。」と表示

			//$_POSTに"Seibetsu"データがあるかチェック
			//isset関数を使う
			if(isset($_POST["Seibetsu"])){
				//"Seibetsuがあれば"チェック
				if($_POST["Seibetsu"] == "0"){
					print "<h2>あなたは<span>男性</span>ですね。</h2>\n";
				}else if($_POST["Seibetsu"] == "1"){
					print "<h2>あなたは<span>女性</span>ですね。</h2>\n";
				}
			}else{
				print "<h2 class=\"red\">性別を選択してください。</h2>\n";
			}

			//年齢計算プログラム
			//現在の日付を取得
			$NowYear = date("Y");
			$NowMonth = date("m");
			$NowDay = date("d");

			if(empty($_POST["year"]) || empty($_POST["month"]) || empty($_POST["day"])){
				print "<h2 class=\"red\">生年月日を入力してください<h2>\n";
			}else{

				//誕生日前の年齢
				$Old = $NowYear - $_POST["year"] -1;

				//誕生日を超えたら＋１歳
				if($NowMonth > $_POST["month"]){
					$Old += 1;
				}else if($NowMonth == $_POST["month"] && $NowDay >= $_POST["day"]){
					$Old += 1;
				}

				// 誕生日を出力
				if($Old == -1){
					print "<h2 class=\"red\">正しい生年月日を入力してください。</h2>\n";
				}else{
					print "<h2>あなたの年齢は<span>{$Old}歳</span>ですね</h2>\n";
				}
			}

			// チェックボックスで選択した趣味を表示
			$hoby = "";
			if(isset($_POST["hoby"])){
					for($i = 0; $i < count($_POST["hoby"]); $i++){
						if($i == 0){
							$hoby = "<span class=\"red\">".$_POST["hoby"][$i]."</span>";
						}else{
							$hoby = $hoby ."と<span class=\"red\">". $_POST["hoby"][$i]."</span>";
						}
					}
					print "<h2>あなたの趣味は".$hoby."ですね</h2>\n";
			}else{
				print "<h2 class=\"red\">この中に趣味はありませんでした</h2>\n";
			}

			//textareaの値を表示
			if(!empty($_POST["pr"])){		//textareaに入力がある場合
				$val = nl2br($_POST["pr"]);
				$val = htmlspecialchars($val);
				print "<h2>".$val."</h2>";
			}else{							//textareaに入力がある場合
				print "<h2 class=\"red\">未入力です</h2>";
			}
		?>
	</body>
</html>