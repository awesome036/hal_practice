<!DOCTYPE html>
<html lang="ja">
	<head>
		<title>誕生日入力</title>
		<meta charset="utf-8">
		<meta name="author" content="Osamu Kurosawa">
	</head>
	<body>
		<form action="./how_old.php" method="post">
			<?php
				//年のセレクトボックス
				$html = "<select name=\"year\">\n";
				$year = date('Y');

				for($i = $year; $year - 100 <= $i; $i--){
					$html .= "<option value=\"$i\">$i</option>\n";
				}
				$html .= "</select>年\n";

				//月のセレクトボックス
				$html .= "<select name=\"month\">\n";

				for($i = 1; $i <= 12; $i++){
					$html .= "<option value=\"$i\">$i</option>\n";
				}
				$html .= "</select>月\n";

				//日のセレクトボックス
				$html .= "<select name=\"day\">\n";

				for($i = 1; $i <= 31; $i++){
					$html .= "<option value=\"$i\">$i</option>\n";
				}
				$html .= "</select>日\n";

				//セレクトボックス出力
				print "$html<br/>\n";
			?>
			<input type="submit" value="送信"/>
			<input type="reset" value="リセット"/>
		</form>
	</body>
</html>
