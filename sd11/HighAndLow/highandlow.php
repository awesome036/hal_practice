<?php
	//セッションスタート
	session_start();

	// 入力値取得
	$number = $_POST["num"];
	// カウント変数
	$CNT = 0;
	// メッセージ変数
	$msg = 0;

	// 正しい数値が入力された場合
	if(preg_match("/^[0-9]{1,3}$/",$number)){
		// セッションにCOUNTの値があったら
		// COUNTの値をCNTに入れる
		if(isset($_SESSION["COUNT"])){
			// COUNTがある場合
			$CNT = $_SESSION["COUNT"];
		}

		// セッションにRANDOMの値があったら
		// RANDOMの値をRANDに入れる
		if(isset($_SESSION["RANDOM"])){
			// RANDOMがある場合
			$RAND = $_SESSION["RANDOM"];
		}else{
			// RANDOMがない場合
			// RANDにランダムな値を入れる
			// RANDをセッションに保存
			$RAND = rand(0,999);
			$_SESSION["RANDOM"] = $RAND;
		}

		// カウントアップ
		// カウント数をセッションに保存
		$CNT++;
		$_SESSION["COUNT"] = $CNT;

		// 数値とRANDを比較
		if($number > $RAND){
			// RANDより大きい場合
			$msg = "<p><span class='blue'>はずれ</span>です！<br/>入力された値({$number})はコンピュータより<span class='red'>High</span>でした。</p>\n";
			$msg .= "<p>判定回数:".$CNT."回</p>\n";
		}elseif($number < $RAND){
			// RANDより小さい場合
			$msg = "<p><span class='blue'>はずれ</span>です！<br/>入力された値({$number})はコンピュータより<span class='blue'>Low</span>でした。</p>\n";
			$msg .= "<p>判定回数:".$CNT."回</p>\n";
		}else{
			// 一致した場合
			$msg = "<p class='red'>当たりです！</p>";
			$msg .= "<p>判定回数:".$CNT."回</p>\n";
			$msg .= "<p>コンピュータの値は".$RAND."でした。</p>\n";

			// セッションの値を削除
			unset($_SESSION["COUNT"]);
			unset($_SESSION["RANDOM"]);
		}
	}else{
		// 数値以外が入力された場合
		$msg = "<h2>入力された値が不正です。<br/>入力値は0~999までで入力してください。</h2>\n";
	}

?>
<!DOCTYPE html>
<html lang="ja">
	<head>
		<title>結果</title>
		<meta charset="utf-8" />
		<meta name="author" content="Osamu Kurosawa" />
		<style type="text/css">
			* {
				color: #333;
			}
			html,
			body {
				margin: 0;
				padding: 0;
			}
			body {
				text-align: center;
			}
			#content {
				width: 80%;
				margin: 0 auto;
				padding: 30px;
				border: solid 3px #e60012;
			}
			.rainbow {
				animation-name: rainbow;
				animation-iteration-count: infinite;
				animation-duration: 5s;
			}
			.button {
				text-decoration: none;
				background-color: #f92;
				border: solid 2px #f92;
				color: #fff;
				font-size: 1.2em;
				font-weight: bold;
				padding: 5px 10px;
				border-radius: 5px;
				transition: .2s ease-in;
			}
			.button:hover {
				cursor: pointer;
				background: #fff;
				color: #f92;
			}
			.red {
				color: #f00;
			}
			.blue {
				color: #00f;
			}

			/*-------------------------------------------*/
			@keyframes rainbow {
				0% {
					border-color: #e60012;
				}
				10% {
					border-color: #f39800;
				}
				20% {
					border-color: #fff100;
				}
				40% {
					border-color: #009944;
				}
				60% {
					border-color: #0068b7;
				}
				80% {
					border-color: #1d2088;
				}
				90% {
					border-color: #920783;
				}
				100% {
					border-color: #e60012;
				}
			}
		</style>
	</head>
	<body>
		<h1>HighAndLowゲーム</h1>
		<div id="content" class="rainbow">
			<?php print $msg; ?>
			<a class="button" href="./input.html">戻る</a>
	</div>
	</body>
</html>