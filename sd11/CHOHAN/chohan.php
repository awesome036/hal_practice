<!DOCTYPE html>
<html lang="ja">
	<head>
		<title>丁半ゲーム</title>
		<meta charset="utf-8">
		<style type="text/css">
			.red {
				color: #f00;
			}
			.blue {
				color: #00f;
			}
			a {
				text-decoration: none;
				background-color: #fff;
				color: #333;
				border: solid 2px #333;
				border-radius: 25px;
				padding: 7px 14px;
				transition: .2s ease-in;
			}
			a:hover {
				background-color: #333;
				color: #fff;
			}
		</style>
	</head>
	<body>
		<?php
			// セッションの生成
			session_start();

			if(isset($_POST["reset"])){
				// リセットボタンが押された場合の処理
				// 回数をリセット（セッションから削除）
				unset($_SESSION["Cnt"]);

				// 勝ち数をリセット（セッションから削除）
				unset($_SESSION["WinCnt"]);

				print "<h2>リセットしました！</h2>";
			}elseif(isset($_POST["submit"])){
				// 勝負ボタンが押された場合の処理
				// ゲーム回数
				$Cnt = 0;

				// セッションにゲーム回数がある場合は、ゲーム回数を取り出す
				if(isset($_SESSION["Cnt"])){
					$Cnt = $_SESSION["Cnt"];
				}

				// 勝ち数
				$WinCnt = 0;

				// セッションに勝ち数がある場合は、勝ち数を取り出す
				if(isset($_SESSION["WinCnt"])){
					$WinCnt = $_SESSION["WinCnt"];
				}

				print "<h1>結果</h1>";

				if(isset($_POST["chohanVal"])){
					// 入力値がある場合

					// ゲーム回数を＋1
					$Cnt++;

					$dice1 = rand(1,6);
					$dice2 = rand(1,6);
					$modval = ($dice1 + $dice2) % 2;

					// パターン１
					if($_POST["chohanVal"] == $modval){
						$msg = "<span class='red'>当たり</span>です！";
						// 勝ち数を＋1
						$WinCnt++;
					}else{
						$msg = "<span class='blue'>はずれ</span>です！";
					}

					if($modval == 1){
						$ret = "半";
					}else{
						$ret = "丁";
					}

					// セッションに回数と勝ち数を保存
					$_SESSION["Cnt"] = $Cnt;
					$_SESSION["WinCnt"] = $WinCnt;

					// 当たり率
					$Hit = 100 * $WinCnt / $Cnt;
					$msg = $msg."<br/>当たり率 = ".$Hit."%<br/>";

					print "<h2>サイコロ１の目は{$dice1}です。</h2>";
					print "<h2>サイコロ２の目は{$dice2}です。</h2>";
					print "<h2>出た目は{$ret}でした。</h2>";
					print "<h2>".$msg."</h2>";
					print "<h2>(回数={$_SESSION["Cnt"]}:当たり回数={$_SESSION["WinCnt"]})</h2>";

					/* パターン２
					$msg[0][0] = "出た目は丁でした。<br/><br/><span class='red'>当たり</span>です！";
					$msg[0][1] = "出た目は半でした。<br/><br/><span class='blue'>はずれ</span>です！";
					$msg[1][0] = "出た目は丁でした。<br/><br/><span class='blue'>はずれ</span>です！";
					$msg[1][1] = "出た目は半でした。<br/><br/><span class='red'>当たり</span>です！";


					print "<h2>サイコロ１の目は{$dice1}です。</h2>";
					print "<h2>サイコロ２の目は{$dice2}です。</h2>";
					print "<h2>".$msg[$_POST["chohanVal"]][$modval]."</h2>";
					*/
				}else{
					// 入力値がない場合
					print "<h2 class='red'>丁か半を選択してください！</h2>";
				}
			}// 勝負ボタンが押された場合の処理ここまで

			print "<a href='input.html'>戻る</a>";
		?>
	</body>
</html>