function init() {
	setInterval(dateTimeDisplay, 500);
}

function dateTimeDisplay(){
	var now = new Date();
	var weeks = new Array('日','月','火','水','木','金','土');

	var yyyy = now.getFullYear();
	var MM = now.getMonth() + 1;
	var dd = now.getDate();
	var wk = weeks[now.getDay()];
	var hh = now.getHours();
	var mm = now.getMinutes();
	var ss = now.getSeconds();

	var yyyy_UTC = now.getUTCFullYear();
	var MM_UTC = now.getUTCMonth() + 1;
	var dd_UTC = now.getUTCDate();
	var wk_UTC = weeks[now.getUTCDay()];
	var hh_UTC = now.getUTCHours();
	var mm_UTC = now.getUTCMinutes();
	var ss_UTC = now.getUTCSeconds();

	if (MM < 10){ MM = "0" + MM;}
	if (dd < 10) { dd = "0" + dd; }
	if (hh < 10) { hh = "0" + hh; }
	if (mm < 10) { mm = "0" + mm; }
	if (ss < 10) { ss = "0" + ss; }

	if (MM_UTC < 10) { MM_UTC = "0" + MM_UTC; }
	if (dd_UTC < 10) { dd_UTC = "0" + dd_UTC; }
	if (hh_UTC < 10) { hh_UTC = "0" + hh_UTC; }
	if (mm_UTC < 10) { mm_UTC = "0" + mm_UTC; }
	if (ss_UTC < 10) { ss_UTC = "0" + ss_UTC; }

	var dt = yyyy + "-" + MM + "-" + dd + "("+ wk + ") " + hh + ":" + mm + ":" + ss;
	document.getElementById("dTime").innerHTML = dt;

	var dt_UTC = yyyy_UTC + "-" + MM_UTC + "-" + dd_UTC + "(" + wk_UTC + ") " + hh_UTC + ":" + mm_UTC + ":" + ss_UTC;
	document.getElementById("dTime_UTC").innerHTML = dt_UTC;

}