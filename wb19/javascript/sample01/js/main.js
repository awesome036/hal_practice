function init() {
	setInterval(dateTimeDisplay, 500);
}

function dateTimeDisplay(){
	var now = new Date();
	var weeks = new Array('日','月','火','水','木','金','土');

	var yyyy = now.getFullYear();
	var MM = now.getMonth() + 1;
	var dd = now.getDate();
	var wk = weeks[now.getDay()];
	var hh = now.getHours();
	var mm = now.getMinutes();
	var ss = now.getSeconds();

	if (MM < 10){ MM = "0" + MM;}
	if (dd < 10) { dd = "0" + dd; }
	if (hh < 10) { hh = "0" + hh; }
	if (mm < 10) { mm = "0" + mm; }
	if (ss < 10) { ss = "0" + ss; }

	var dt = yyyy + "-" + MM + "-" + dd + "("+ wk + ") " + hh + ":" + mm + ":" + ss;
	document.getElementById("dTime").innerHTML = dt;

}