function clock(){
	var picture=[
		"./images/0.png", "./images/1.png", "./images/2.png", "./images/3.png", "./images/4.png",
		"./images/5.png", "./images/6.png", "./images/7.png", "./images/8.png", "./images/9.png"
	];

	var jikan = new Date();
	var hour = jikan.getHours();
	var minute = jikan.getMinutes();
	var second = jikan.getSeconds();

	var h1 = picture[ Math.floor(hour/10) ];
	var h2 = picture[ hour % 10 ];
	var m1 = picture[ Math.floor(minute / 10) ];
	var m2 = picture[ minute % 10 ];
	var s1 = picture[ Math.floor(second / 10) ];
	var s2 = picture[ second % 10 ];

	document.getElementById("h1").src = h1;
	document.getElementById("h2").src = h2;
	document.getElementById("m1").src = m1;
	document.getElementById("m2").src = m2;
	document.getElementById("s1").src = s1;
	document.getElementById("s2").src = s2;
	
}

function randomClock(){
	var picture = [
		"./images/0.png", "./images/1.png", "./images/2.png", "./images/3.png", "./images/4.png",
		"./images/5.png", "./images/6.png", "./images/7.png", "./images/8.png", "./images/9.png"
	];

	var times=["h1","h2","m1","m2","s1","s2"];
	
	for(var i=0; i<times.length; i++){
		var rand = Math.floor( Math.random()*10);
		document.getElementById(times[i]).src = picture[rand];
	}
}

function tstart(){
	// setInterval(clock, 500);
	setInterval(randomClock,100);
}