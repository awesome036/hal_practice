// while文
// while(条件式){
// 	処理
// }
var i = 1;
while(i<=12){
	console.log(i + "月");
	i++;
}

// do while文
// do{
// 	処理
// }while(条件式);
i = 10;
do{
	if(i%2 == 0){
		document.write("ざわ...");
	}else{
		document.write("ざわざわ...");
	}
	i--;
}while(i>=0);
document.write("圧倒的暴挙！");

// 練習：数値を繰り返し入力させて合計を出力。０が入力されてたら終了