
SET sql_mode='STRICT_ALL_TABLES';

set character_set_client=sjis;
set character_set_connection=utf8;
set character_set_results=sjis;
set character_set_server=utf8;

DROP DATABASE IF EXISTS ph22_shop;
CREATE DATABASE ph22_shop;
USE ph22_shop;

DROP TABLE IF EXISTS staff;
CREATE TABLE staff(
	login_id VARCHAR(10) PRIMARY KEY,
	password_hash VARCHAR(255),
	name VARCHAR(20) NOT NULL
);

DROP TABLE IF EXISTS product;
CREATE TABLE product(
	code VARCHAR(10) PRIMARY KEY,
	name VARCHAR(30) NOT NULL,
	price INT,
	image_name VARCHAR(127) UNIQUE
);