DROP DATABASE IF EXISTS db0226;

SET sql_mode='STRICT_ALL_TABLES';

CREATE DATABASE db0226;
USE db0226;

DROP TABLE timetable;
CREATE TABLE timetable(
	className CHAR(8),
	day ENUM('月','火','水','木','金','土','日'),
 	period INT(1),
	subjectID INT(128),
	roomNum VARCHAR(5),
	teacherID INT,
	PRIMARY KEY(className,day,period)
);

INSERT INTO timetable VALUES('PW51A335','月',3,1,'282',1);
INSERT INTO timetable VALUES('PW51A335','月',4,2,'296',2);
INSERT INTO timetable VALUES('PW51A335','火',1,3,'353',3);
INSERT INTO timetable VALUES('PW51A335','火',2,3,'353',3);
INSERT INTO timetable VALUES('PW51A335','水',1,4,'354',4);
INSERT INTO timetable VALUES('PW51A335','水',2,4,'354',4);
INSERT INTO timetable VALUES('PW51A335','水',3,5,'342',5);
INSERT INTO timetable VALUES('PW51A335','水',4,6,'342',6);
INSERT INTO timetable VALUES('PW51A335','水',5,6,'342',6);
INSERT INTO timetable VALUES('PW51A335','木',1,5,'353',5);
INSERT INTO timetable VALUES('PW51A335','木',2,4,'354',4);
INSERT INTO timetable VALUES('PW51A335','金',2,7,'365',4);
INSERT INTO timetable VALUES('PW51A335','金',3,1,'282',1);
INSERT INTO timetable VALUES('PW51A335','金',4,8,'296',7);
INSERT INTO timetable VALUES('PW51A335','金',5,8,'296',7);
INSERT INTO timetable VALUES('PI51A335','月',3,1,'282',1);
INSERT INTO timetable VALUES('PI51A335','月',4,2,'296',2);
INSERT INTO timetable VALUES('PI51A335','火',1,3,'353',3);
INSERT INTO timetable VALUES('PI51A335','火',2,3,'353',3);
INSERT INTO timetable VALUES('PI51A335','水',1,4,'354',4);
INSERT INTO timetable VALUES('PI51A335','水',2,4,'354',4);
INSERT INTO timetable VALUES('PI51A335','水',3,5,'342',5);
INSERT INTO timetable VALUES('PI51A335','水',4,6,'342',6);
INSERT INTO timetable VALUES('PI51A335','水',5,6,'342',6);
INSERT INTO timetable VALUES('PI51A335','木',1,5,'353',5);
INSERT INTO timetable VALUES('PI51A335','木',2,4,'354',4);
INSERT INTO timetable VALUES('PI51A335','金',2,7,'365',4);
INSERT INTO timetable VALUES('PI51A335','金',3,1,'282',1);
INSERT INTO timetable VALUES('PI51A335','金',4,8,'296',7);
INSERT INTO timetable VALUES('PI51A335','金',5,8,'296',7);

DROP TABLE subjects;
CREATE TABLE subjects(
	subjectID INT(128) PRIMARY KEY AUTO_INCREMENT,
	subjectCode VARCHAR(4),
	subjectName VARCHAR(30)
);

INSERT INTO subjects(subjectCode,subjectName)VALUES('CS19','コンピュータサイエンス1');
INSERT INTO subjects(subjectCode,subjectName)VALUES('MK19','コンセプトメイキング');
INSERT INTO subjects(subjectCode,subjectName)VALUES('NT19','ネットワーク1');
INSERT INTO subjects(subjectCode,subjectName)VALUES('SD19','Webシステム開発');
INSERT INTO subjects(subjectCode,subjectName)VALUES('PH19','PHPプログラミング2');
INSERT INTO subjects(subjectCode,subjectName)VALUES('WB19','Webデザイン制作2');
INSERT INTO subjects(subjectCode,subjectName)VALUES('OT1C','フレックススタディー');
INSERT INTO subjects(subjectCode,subjectName)VALUES('DB19','データベース構築2');

DROP TABLE teachers;
CREATE TABLE teachers(
	teacherID INT PRIMARY KEY AUTO_INCREMENT,
	teacherName VARCHAR(20)
);

INSERT INTO teachers(teacherName)VALUES('兼平敦');
INSERT INTO teachers(teacherName)VALUES('桃井聖司');
INSERT INTO teachers(teacherName)VALUES('足立篤治');
INSERT INTO teachers(teacherName)VALUES('中川勲俊');
INSERT INTO teachers(teacherName)VALUES('佐藤祐勝');
INSERT INTO teachers(teacherName)VALUES('奥野雅之');
INSERT INTO teachers(teacherName)VALUES('田坂啓輔');

-- 時間割出力
SELECT className, day, period, subjectCode, roomNum, teacherName FROM timetable
JOIN subjects ON timetable.subjectID = subjects.subjectID
JOIN teachers ON timetable.teacherID = teachers.teacherID
ORDER BY className,day,period
;