DROP DATABASE IF EXISTS test;
CREATE DATABASE test CHARACTER SET UTF8;

USE test

CREATE TABLE T_01(
	F_id INT
	,F_name VARCHAR(16) DEFAULT 'hal123'
);

CREATE TABLE T_02(
	F_id INT
	,F_no INT
	,F_class VARCHAR(16)
	,F_name VARCHAR(16)
	,UNIQUE(F_no,F_class)
);

INSERT INTO T_01 VALUES
	(1,"hoge")
	,(2,"hoge")
	,(3,"hoge")
--	,(4,"hoge")
	,(5,"hoge")
	,(6,"hoge")
;

-- 外部キーの書き方
CREATE TABLE t_15(
	period int,
	week int,
	name varchar(16),
	FOREIGN KEY (name) REFERENCES t_13(name)
);