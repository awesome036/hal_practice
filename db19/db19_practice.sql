DROP DATABASE IF EXISTS db_practice;

SET character_set_client=sjis;
SET character_set_connection=utf8;
SET character_set_server=utf8;
SET character_set_results=sjis;

SET sql_mode='STRICT_ALL_TABLES';

CREATE DATABASE db_practice;
USE db_practice

DROP TABLE IF EXISTS T_productList;
CREATE TABLE T_productList(
	F_id INT PRIMARY KEY AUTO_INCREMENT,
	F_name VARCHAR(20) NOT NULL UNIQUE,
	F_price INT NOT NULL
);

INSERT INTO T_productList(F_name,F_price) VALUES
	('うまい棒ソース味',10)
	,('うまい棒たこ焼き味',10)
	,('うまい棒ハンバーガー味',10)
	,('うまい棒サラミ味',10)
	,('うまい棒納豆味',10)
	,('うまい棒マヨネーズ味',10)
	,('うまい棒明太子味',10)
	,('うまい棒サラダ味',10)
	,('うまい棒ナポリタン味',10)
	,('うまい棒ラスク味',10)
;

DROP TABLE IF EXISTS T_customerList;
CREATE TABLE T_customerList(
	F_id INT PRIMARY KEY AUTO_INCREMENT,
	F_name VARCHAR(16) NOT NULL UNIQUE,
	F_address VARCHAR(50) NOT NULL,
	F_rank CHAR(2) DEFAULT 'd'
);

INSERT INTO T_customerList(F_name,F_address) VALUES
	('田中太郎','東京都練馬区')
	,('佐藤幸一','北海道朝日町')
	,('鈴木一郎','沖縄県那覇市')
	,('渡辺三郎','茨城県つくば市')
	,('吉田次郎','神奈川県横浜市')
;

DROP TABLE IF EXISTS T_order;
CREATE TABLE T_order(
	F_orderNo INT PRIMARY KEY AUTO_INCREMENT,
	F_customerID INT NOT NULL,
	F_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY(F_customerID) REFERENCES T_customerList(F_id)
);

INSERT INTO T_order(F_customerID) VALUES(2),(3),(4),(1),(5),(2);

DROP TABLE IF EXISTS T_orderList;
CREATE TABLE T_orderList(
	F_orderNo INT NOT NULL,
	F_productID INT NOT NULL,
	F_quantity INT NOT NULL,
	PRIMARY KEY(F_orderNo,F_productID),
	FOREIGN KEY(F_orderNo) REFERENCES T_order(F_orderNo),
	FOREIGN KEY(F_productID) REFERENCES T_productList(F_id)
);

INSERT INTO T_orderList VALUES
	(1,3,200)
	,(1,5,25)
	,(2,1,300)
	,(3,4,10)
	,(3,5,10)
	,(4,2,20)
	,(5,5,500)
	,(6,3,20)
	,(6,5,30)
;

CREATE VIEW T_orderedProducts AS
SELECT F_orderNo AS '注文番号',
		F_Name AS '商品名',
		F_quantity AS '数量',
		F_price AS '単価',
		F_quantity * F_price AS '金額'
FROM T_orderList,T_productList
WHERE T_orderList.F_productID = T_productList.F_id;