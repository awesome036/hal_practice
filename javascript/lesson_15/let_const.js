// var i = 10; // 宣言OK
// i = 11; // 再代入OK
// var i = 12;　// 再宣言OK

// let j = 10; // 宣言OK
// j = 11; //　再代入OK
// let j = 12; //再宣言NG

const tax_rate = 0.08;  //　宣言OK
tax_rate = 0.1; // 再代入NG
const tax_rate = 0.2    // 再宣言NG