// // 従来の関数
// let f = function(x,y){
//     return x * y;
// }
// console.log(f(2,3));

// // アロー関数
// let f = (x,y) => {
//     return x * y;
// }
// console.log(f(2,3));

// 引数がない場合は省略不可
let f = () => {
    console.log('test');
}
f();